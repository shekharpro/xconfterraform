provider "aws" {
  region = "${var.aws_region}"
  shared_credentials_file = "${var.credentials_file}"
  profile = "${var.credentials_profile}"
}

module "vpc_stack" {
  source = "vpc_stack"
  vpc_cidr = "${var.vpc_cidr}"
  vpc_name = "${var.project_name}-${var.environment}"

  public_subnet_name = "public-${var.project_name}-${var.environment}"
  public_subnet_az = "${var.availability_zone_a}"
  public_subnet_cidr = "${var.sbn_public_cidr}"

  private_subnet_name = "private-${var.project_name}-${var.environment}"
  private_subnet_az = "${var.availability_zone_a}"
  private_subnet_cidr = "${var.sbn_private_cidr}"
}

# Create an internet gateway to give our subnet access to the outside world
resource "aws_internet_gateway" "default" {
  vpc_id = "${module.vpc_stack.vpc_id}"
  tags {
    Name = "${var.project_name}-${var.environment}"
  }
}

# Grant the VPC internet access on its main route table
resource "aws_route" "internet_access" {
  route_table_id = "${module.vpc_stack.vpc_main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = "${aws_internet_gateway.default.id}"
}

#Elastic IP for NAT
resource "aws_eip" "nat" {
  vpc = true
}

#Nat Gateway association to EIP and Subnet in which to create (public in our case)
resource "aws_nat_gateway" "default" {
  allocation_id = "${aws_eip.nat.id}"
  subnet_id = "${module.vpc_stack.public_subnet_id}"
  depends_on = [
    "aws_internet_gateway.default"]
}

resource "aws_route_table" "private" {
  vpc_id = "${module.vpc_stack.vpc_id}"
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.default.id}"
  }
  depends_on = [
    "module.vpc_stack",
    "aws_nat_gateway.default"]
  tags {
    Name = "private-${var.project_name}-${var.environment}"
  }
}

#Associate it to Private Subnet
resource "aws_route_table_association" "private" {
  subnet_id = "${module.vpc_stack.private_subnet_id}"
  route_table_id = "${aws_route_table.private.id}"
  depends_on = [
    "module.vpc_stack"]
}

#########################################################################

# instances security group SSH, HTTP
resource "aws_security_group" "web" {
  name = "web-${var.project_name}-${var.environment}"
  description = "web-${var.project_name}-${var.environment} server instances security group with HTTP and SSH"
  vpc_id = "${module.vpc_stack.vpc_id}"

  # SSH access from internet (!!!)
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  # HTTP access from internet
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  # outbound access to anywhere
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  tags {
    Name = "web-${var.project_name}-${var.environment}"
  }
}

# instances security group MongoDB default port and webstatus
resource "aws_security_group" "db" {
  name = "db-${var.project_name}-${var.environment}"
  description = "db-${var.project_name}-${var.environment} server instances security group with MongoDB ports"
  vpc_id = "${module.vpc_stack.vpc_id}"

  # DB access from internet (!!!)
  ingress {
    from_port = 27017
    to_port = 27017
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  # WebStatus page access from internet
  ingress {
    from_port = 28017
    to_port = 28017
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  # outbound access to anywhere
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  tags {
    Name = "sg-db-${var.project_name}-${var.environment}"
  }
}

#########################################################################
# EC2 Instance DB
resource "aws_instance" "db" {
  ami = "${var.centos_ami}"
  instance_type = "t2.micro"
  key_name = "xconftalk"
  subnet_id = "${module.vpc_stack.private_subnet_id}"
  vpc_security_group_ids = [
    "${aws_security_group.db.id}"]
  user_data = <<EOF
sudo yum update -y
sudo echo "[mongodb-org-3.2]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.2/x86_64/
gpgcheck=0
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-3.2.asc" >> /etc/yum.repos.d/mongo-repo.repo
sudo yum install mongodb-org -y
sudo service mongod start
EOF

  tags {
    Name = "db-${var.project_name}-${var.environment}"
  }
}

# EC2 Instance Web
resource "aws_instance" "web" {
  count = 1
  ami = "${var.centos_ami}"
  instance_type = "t2.micro"
  key_name = "xconftalk"
  subnet_id = "${module.vpc_stack.public_subnet_id}"
  vpc_security_group_ids = [
    "${aws_security_group.web.id}"]

  tags {
    Name = "web-${var.project_name}-${var.environment}"
  }
  vpc_security_group_ids = [
    "${aws_security_group.db.id}"]
  user_data = <<EOF
sudo echo "[mongodb-org-3.2]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.2/x86_64/
gpgcheck=0
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-3.2.asc" >> /etc/yum.repos.d/mongo-repo.repo
sudo yum install mongodb-org -y
sudo service mongod start
EOF
  provisioner "remote-exec" {
    inline = [
      "echo 'I was here in web ${aws_instance.web.public_ip}' > ~/iwashere.txt",
      "sudo yum update -y",
      "sudo yum install git -y",
      "sudo yum install epel-release nodejs -y",
      "sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports 8080",
      "git clone https://github.com/shekharpro/node-todo.git",
      "cd node-todo/",
      "npm install",
      "DB_SERVER_URL='${aws_instance.db.private_ip}:27017' PORT=8080 sudo node server.js"
    ]

    connection {
      type = "ssh"
      user = "centos"
      private_key = "${file(var.keyfile)}"
    }
  }
}