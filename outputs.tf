output "vpc" {
  value = "${module.vpc_stack.vpc_id}"
}

output "vpc_route_table" {
  value = "${module.vpc_stack.vpc_main_route_table_id}"
}

output "public_subnet" {
  value = "${module.vpc_stack.public_subnet_id}"
}

output "private_subnet" {
  value = "${module.vpc_stack.private_subnet_id}"
}

output "internet_gateway" {
  value = "${aws_internet_gateway.default.id}"
}

output "route" {
  value = "${aws_route.internet_access.id}"
}

output "nat_eip" {
  value = "${aws_eip.nat.id}"
}

output "nat" {
  value = "${aws_nat_gateway.default.id}"
}

output "web_public_ip" {
  value = "${aws_instance.web.public_ip}"
}

output "web_private_ip" {
  value = "${aws_instance.web.private_ip}"
}

output "db_private_ip" {
  value = "${aws_instance.db.private_ip}"
}

output "nat_private_ip" {
  value = "${aws_nat_gateway.default.private_ip}"
}

output "nat_public_ip" {
  value = "${aws_nat_gateway.default.public_ip}"
}