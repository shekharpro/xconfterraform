credentials_file = "/Users/samajs/.awsconfig/credentials"
credentials_profile = "xconf"
project_name = "XConf"
keyfile = "/Users/samajs/.ssh/xconftalk.pem"
environment = "Demo"
aws_region = "eu-west-1"
availability_zone_a = "eu-west-1a"
availability_zone_b = "eu-west-1b"
availability_zone_c = "eu-west-1c"

vpc_cidr = "10.0.0.0/16"
sbn_public_cidr = "10.0.0.0/24"
sbn_private_cidr = "10.0.1.0/24"
centos_ami = "ami-7abd0209"


