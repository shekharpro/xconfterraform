variable "region" {
  test_update = "eu-west-1"
}
variable "availability_zone_a" {
}
variable "availability_zone_b" {
}
variable "availability_zone_c" {
}
variable "vpc_cidr" {
}
variable "credentials_file" {
}
variable "credentials_profile" {
}
variable "project_name" {
}
variable "keyfile" {
}
variable "environment" {
}
variable "aws_region" {
}
variable "sbn_public_cidr" {
}
variable "sbn_private_cidr" {
}
variable "centos_ami" {
}