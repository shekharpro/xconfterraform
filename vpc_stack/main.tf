# Create a VPC to launch our instances into
resource "aws_vpc" "vpc" {
  cidr_block = "${var.vpc_cidr}"
  tags {
    Name = "${var.vpc_name}"
  }
}

resource "aws_subnet" "public" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "${var.public_subnet_cidr}"
  availability_zone = "${var.public_subnet_az}"
  map_public_ip_on_launch = true
  tags {
    Name = "${var.public_subnet_name}"
  }
}
resource "aws_subnet" "private" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "${var.private_subnet_cidr}"
  availability_zone = "${var.private_subnet_az}"
  map_public_ip_on_launch = false
  tags {
    Name = "${var.private_subnet_name}"
  }
}