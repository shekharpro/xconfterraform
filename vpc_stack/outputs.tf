output "vpc_id" {
  value = "${aws_vpc.vpc.id}"
}
output "vpc_main_route_table_id" {
  value = "${aws_vpc.vpc.main_route_table_id}"
}
output "public_subnet_id" {
  value = "${aws_subnet.public.id}"
}
output "private_subnet_id" {
  value = "${aws_subnet.private.id}"
}
