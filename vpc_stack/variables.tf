variable "vpc_name" {
}
variable "vpc_cidr" {
}
variable "public_subnet_name" {
}
variable "public_subnet_az" {
}
variable "public_subnet_cidr" {
}
variable "private_subnet_name" {
}
variable "private_subnet_az" {
}
variable "private_subnet_cidr" {
}